package com.max;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping("/")
    public String home(){

        return "<h1>Welcome</h1>";
    }
    @GetMapping("/user/{user}")
    public String getUser(@PathVariable String user ){
        return "<h1>Welcome user</h1>"+user;
    }
    @GetMapping("/admin")
    public String getAdmin(){
        return "<h1>Welcome admin</h1>";
    }

}
