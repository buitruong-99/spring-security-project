# spring-secutity-project

Basics of spring security authentication and authorization

1.  Security confifuration class by enabling @EnableWebSecurity
2.  and extend WebSecurityConfigurerAdapter
3.  configure AuthenticationManagerBuilder
4.  im-memory authentication configuration used and 
5.  password encoder used for hashing the passwords(here used NoOpPasswordEncoder)
6.  configure HttpSecurity for authorization (use antMAtchers and hasRoles to authorize)

Authentication process

    input credentials -->    
                            Authentication
    output principal <---
    
Multiple authentication stratergies usernm n pswd ,ladap, oauth
    
    ProviderManger impl AuthenticationManager  --> AuthenticationProvider       ---> UserDetailsService
       authenticate()                                   authenticate()                  loadUserByUserName()
                                                        support(Authentication)   ---> usernm/pswd or oauth or ldap
                                                        

Authorization

    Different apis having diff access requirements
    Api         Roles allows to access it
    ----        ---------------------------
    /           permit all(unauthenticated)
    /user       user and admin roles
    /admin      admin role
                    